<?php
/**
 * A simple jabber bot in PHP - to monitor status currently
 * (closely based on "edgar" : http://edgar.netflint.net/ )
 *
 * @author James Walker <james@bryght.com>
 */

// some trickery to get multisite working properly
$_SERVER['PHP_SELF'] = '/'.basename($_SERVER['argv'][0]);
if ($_SERVER['argv'][1]) {
  $_SERVER['HTTP_HOST'] = $_SERVER['argv'][1];
}
require_once 'includes/bootstrap.inc';
require_once dirname(__FILE__) . '/class.jabber.php';

$JABBER = new Jabber;

$JABBER->server         = variable_get('jabber_bot_server', '');
$JABBER->host           = (variable_get('jabber_bot_host', '')) ? variable_get('jabber_bot_host', '') : $JABBER->server;
$JABBER->port           = 5222;
$JABBER->username       = variable_get('jabber_bot_username', '');
$JABBER->password       = variable_get('jabber_bot_password', '');
$JABBER->resource       = "DrupalJabberBot";
$JABBER->use_ssl        = FALSE;

$JABBER->enable_logging = TRUE;
$JABBER->log_filename   = '/tmp/jabber-bot-' . $JABBER->server .'.log';


$JABBER->Connect() or die("Couldn't connect!\n");
$JABBER->SendAuth() or die("Couldn't authenticate!\n");
$JABBER->SendPresence();

while (1) {
  process_registrations();
  $JABBER->CruiseControl(360);
  if (!($JABBER->connected)) {
    $JABBER->Disconnect();
    sleep(20);
    $JABBER->Connect();
    $JABBER->SendAuth();
    $JABBER->SendPresence();
    sleep(5);
  }
}

function Handler_presence_available($message){
  global $JABBER;
  $jid = $JABBER->StripJID($JABBER->GetInfoFromPresenceFrom($message));
  $status = $JABBER->GetInfoFromPresenceShow($message);
  if (!$status) {
    $status = 'available';
  }
  $message = $JABBER->GetInfoFromPresenceStatus($message);
  db_query("UPDATE {jabber} SET status='%s', message='%s' WHERE jid='%s'", $status, $message, $jid);
}

function Handler_presence_unavailable($message) {
  global $JABBER;
  $jid = $JABBER->StripJID($JABBER->GetInfoFromPresenceFrom($message));
  db_query("UPDATE {jabber} SET status='offline' WHERE jid='%s'", $jid);
}

function Handler_presence_subscribe($message) {
  global $JABBER;
  $jid = $JABBER->StripJID($JABBER->GetInfoFromPresenceFrom($message));
  $JABBER->SubscriptionAcceptRequest($jid);
  $JABBER->Subscribe($jid);
}

function Handler_presence_subscribed($message) {
  global $JABBER;
  $jid = $JABBER->StripJID($JABBER->GetInfoFromPresenceFrom($message));
}

function Handler_presence_error() {
}

function Handler_iq_($message = null) {
    var_dump($message);
}

function process_registrations() {
  global $JABBER;
  $result = db_query("SELECT * FROM {jabber} WHERE status = 'unsubscribed' OR status='pending'");
  while ($row = db_fetch_object($result)) {
    $JABBER->Subscribe($row->jid);
    $JABBER->SubscriptionAcceptRequest($row->jid);
    db_query("UPDATE {jabber} SET status='pending' WHERE jid='%s'", $row->jid);
  }
}

?>
